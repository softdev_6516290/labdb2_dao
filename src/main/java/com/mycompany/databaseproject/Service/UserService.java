/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.Service;

import com.mycompany.databaseproject.model.Dao.UserDao;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author Acer
 */
public class UserService {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = new UserDao().getByName(name);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
}
