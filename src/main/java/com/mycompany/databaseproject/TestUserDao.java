/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.Dao.UserDao;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author Acer
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u: userDao.getall()){
            System.out.println(u);
        }
        System.out.println("\n");
        User user1 = userDao.get(2);
        System.out.println(user1);
        
        System.out.println("\n");
        //User newUser = new User("User3", "password", 2, "F");
        //User insertedUser = userDao.save(newUser);
        //System.out.println(insertedUser);
        //insertedUser.setGender("M");
        //user1.setGender("F");
        //userDao.update(user1);
        //User updateUser = userDao.get(user1.getId());
        //System.out.println(updateUser);
        
        //userDao.delete(user1);
       // for(User u: userDao.getall()){
        //    System.out.println(u);
        //}
        
        
        for(User u: userDao.getAll(" USER_NAME like 'u%'"," USER_NAME asc, USER_GENDER desc")){
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
